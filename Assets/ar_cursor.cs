using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ar_cursor : MonoBehaviour
{

    public GameObject cursorChildObject;
    public GameObject objectToPlace;
    public ARRaycastManager raycastManager;
    private GameObject gameObjectCreated;
    public bool useCursor = true;
    
    // Start is called before the first frame update
    void Start()
    {
        cursorChildObject.SetActive(useCursor);

        DeepLinkGameObject();
    }

    // Update is called once per frame
    void Update()
    {
        if (useCursor)
        {
            UpdateCursor();
        }
        if (Input.touchCount>0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (gameObjectCreated != null)
            {
                GameObject.Destroy(GameObject.Find(gameObjectCreated.name));
                gameObjectCreated = null;
            }
            if (useCursor)
            {
                gameObjectCreated =  GameObject.Instantiate(objectToPlace, transform.position, transform.rotation);
                gameObjectCreated.SetActive(true);
            }
            else
            {
                List<ARRaycastHit> hits = new List<ARRaycastHit>();
                raycastManager.Raycast(Input.GetTouch(0).position, hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);
                if(hits.Count >  0 )
                {
                    gameObjectCreated = GameObject.Instantiate(objectToPlace, hits[0].pose.position, hits[0].pose.rotation);
                }
            }
           
        }
    }
    
    void UpdateCursor()
    {
        Vector2 screenPosition = Camera.main.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        raycastManager.Raycast(screenPosition, hits, UnityEngine.XR.ARSubsystems.TrackableType.Planes);

        if (hits.Count > 0)
        {
            transform.position = hits[0].pose.position;
            transform.rotation = hits[0].pose.rotation;
        }
    }

    void DeepLinkGameObject()
    {
        string url = Application.absoluteURL;
        string gameObjectName = GetGameObjectNameFromUrl(url);
        //gameObjectName = "Cube";
        if (!string.IsNullOrEmpty(gameObjectName))
        {
            GameObject gameObject = Resources.Load<GameObject>(gameObjectName);
            Debug.Log("GameObject created: " + gameObject.name);
            if (gameObject != null)
            {
                objectToPlace = Instantiate(gameObject);
                objectToPlace.SetActive(false);
            }
        }
    }
    string GetGameObjectNameFromUrl(string url)
    {
        string gameObjectParam = "gameObject=";

        if (url.Contains(gameObjectParam))
        {
            int gameObjectParamIndex = url.IndexOf(gameObjectParam) + gameObjectParam.Length;
            string gameObjectName = url.Substring(gameObjectParamIndex);

            return gameObjectName;
        }

        return null;
    }
}
